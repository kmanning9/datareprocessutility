package com.wellcentive;

/**
 * Created with IntelliJ IDEA.
 * User: chrismanning
 * Date: 10/24/13
 * Time: 10:30 AM
 */

import java.io.*;
import java.util.*;
import java.util.regex.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import ca.uhn.hl7v2.model.v23.group.ORU_R01_ORDER_OBSERVATION;
import ca.uhn.hl7v2.model.v23.message.ORU_R01;
import ca.uhn.hl7v2.util.Hl7InputStreamMessageIterator;
import au.com.bytecode.opencsv.CSVWriter;
import com.berryworks.edireader.demo.*;
import org.w3c.dom.*;

public class DataReprocessUtility {
    public static final int YEAR = 0;
    public static final int MONTH = 1;
    public static final int DAY = 2;

    public static void main(String[] args) {
        if(args.length == 0) {
            System.out.println("Not enough arguments.");
            displayProperUsage();
            System.out.println(1);
        } else if(args[0].equalsIgnoreCase("retrieve")) {
            if(args.length != 5) {
                System.out.println("Wrong number of arguments.");
                displayProperUsage();
                System.exit(1);
            }

            String interfaceId = args[1];
            String startDate[] = args[2].split("-");
            String endDate[] = args[3].split("-");
            String outFolder = args[4];

            //attempt to retrieve all
            processRetrieve("/msg/dhub/archives/", true, interfaceId, startDate, endDate, outFolder);
            processRetrieve("/buf/dhub/interfaces/" + interfaceId + "/archive/", false, interfaceId, startDate, endDate, outFolder);
            processRetrieve("/buf/dhub/archives/", true, interfaceId, startDate, endDate, outFolder);
        } else if(args[0].equalsIgnoreCase("make")) {
            if(args.length < 4) {
                System.out.println("Not enough arguments.");
                displayProperUsage();
                System.exit(1);
            }
            if(args[1].equalsIgnoreCase("delimited")) {
                String inputFolder = args[2];
                String outputFile = args[3];
                if(args.length < 5) {
                    System.out.println("No delimiter specified.\nFormat is make delimited [input folder] [output file] [delimiter]");
                    System.exit(1);
                }
                String delimiter = args[4];
                processMakeDelimited(inputFolder, outputFile, Pattern.quote(delimiter), "LABS");
            }
            else if(args[1].equalsIgnoreCase("hl7")) {
                String inputFolder = args[2];
                String outputFile = args[3];
                processMakeHL7(inputFolder, outputFile);
            }
            else {
                System.out.println("That file type is not yet supported.");
                displayProperUsage();
                System.exit(1);
            }
        } else if(args[0].equalsIgnoreCase("copy")) {
            //copies all files from a given list to another directory
            if(args.length < 4) {
                System.out.println("Not enough arguments.");
                displayProperUsage();
                System.exit(1);
            }
            String inputFile = args[1];
            String inputFolder = args[2];
            String outputFolder = args[3];
            copyFileFromList(inputFile, inputFolder, outputFolder);
        } else if(args[0].equalsIgnoreCase("test")) {
            try {
                File f = new File(args[1]);
                StringBuffer sb = new StringBuffer();
                Scanner sc = new Scanner(f);
                while(sc.hasNextLine())
                    sb.append(sc.nextLine() + "\n");
                LinkedList<String> messages = splitX12Messages(sb.toString());
                for(String msg: messages) {
                    System.out.println(msg);
                    System.out.println("-------------------");
                }
            } catch(Exception e) {}
        }
    }

    public static void displayProperUsage() {
        System.out.println("Usage: DataReprocessUtility [command]");
        System.out.println("** All dates in the format of yyyy-mm-dd");
        System.out.println("--------------------------------------");
        System.out.println("List of commands:");
        System.out.println("\tretrieve [interface id] [start date] [end date] [output folder]");
        System.out.println("\tmake [csv | hl7] [input folder] [output file]");
        System.out.println("\tcopy [input file**] [input folder] [output file]");
        System.out.println();
        System.out.println("** Input file should contain one file per line (indicating which files to move to a specified directory)");
    }

    public static boolean valueIsBetweenInclusive(int value, int low, int high) {
        return (value >= low && value <= high);
    }

    //assumes that the dates entered are valid dates
    public static boolean yearIsIncluded(int year, String[] startDate, String[] endDate) throws NumberFormatException {
        return (year >= Integer.parseInt(startDate[YEAR]) && year <= Integer.parseInt(endDate[YEAR]));
    }

    //assumes that the dates entered are valid dates
    public static boolean monthIsIncluded(int year, int month, String[] startDate, String[] endDate) throws NumberFormatException {
        int startYear = Integer.parseInt(startDate[YEAR]);
        int startMonth = Integer.parseInt(startDate[MONTH]);
        int endYear = Integer.parseInt(endDate[YEAR]);
        int endMonth = Integer.parseInt(endDate[MONTH]);

        if(valueIsBetweenInclusive(year, startYear, endYear)) {
            if(year == endYear && year == startYear)
                return valueIsBetweenInclusive(month, startMonth, endMonth);
            else if(year == startYear)
                return valueIsBetweenInclusive(month, startMonth, month);
            else if(year == endYear)
                return valueIsBetweenInclusive(month, month, endMonth);
            else
                return true;
        } else
            return false;
    }

    //assumes that the dates entered are valid dates
    public static boolean dayIsIncluded(int year, int month, int day, String[] startDate, String[] endDate) throws NumberFormatException {
        int startYear = Integer.parseInt(startDate[YEAR]);
        int startMonth = Integer.parseInt(startDate[MONTH]);
        int startDay = Integer.parseInt(startDate[DAY]);
        int endYear = Integer.parseInt(endDate[YEAR]);
        int endMonth = Integer.parseInt(endDate[MONTH]);
        int endDay = Integer.parseInt(endDate[DAY]);

        if(monthIsIncluded(year, month, startDate, endDate)) {
            if(year == startYear && year == endYear) {
                if(valueIsBetweenInclusive(month, startMonth, endMonth)) {
                    if(month == startMonth && month == endMonth)
                        return valueIsBetweenInclusive(day, startDay, endDay);
                    else if(month == startMonth)
                        return valueIsBetweenInclusive(day, startDay, 31);
                    else if(month == endMonth)
                        return valueIsBetweenInclusive(day, 1, endDay);
                    else
                        return true;
                } else
                    return false;
            } else if(year == startYear) {
                if(valueIsBetweenInclusive(month, startMonth, 12)) {
                    if(month == startMonth)
                        return valueIsBetweenInclusive(day, startDay, 31);
                    else
                        return true;
                } else
                    return false;
            } else if(year == endYear) {
                if(valueIsBetweenInclusive(month, 1, endMonth)) {
                    if(month == endMonth)
                        return valueIsBetweenInclusive(day, 1, endDay);
                    else
                        return true;
                }
                else
                    return false;
            } else
                return true;
        } else
            return false;
    }

    public static void processRetrieve(String folderName, boolean untar, String interfaceId, String[] startDate, String[] endDate, String outFolderLocation) {
        System.out.println("Checking " + folderName + " for directories...");

        File archiveDirectoryBase = new File(folderName);
        if(archiveDirectoryBase.exists()) {
            File outLocation = new File(outFolderLocation);
            if(!outLocation.exists()) {
                if(!outLocation.mkdir()) {
                    System.out.println("Unable to create out folder: " + outLocation.getAbsolutePath());
                    return;
                }
            }

            System.out.println("Output folder is " + outLocation.getAbsolutePath());
            File[] yearFolders = archiveDirectoryBase.listFiles();
            if(yearFolders != null) {
                for(File year : yearFolders) {
                    if(year.isDirectory()) {
                        try {
                            int currentYear = Integer.parseInt(year.getName());
                            if(!yearIsIncluded(currentYear, startDate, endDate))
                                continue;
                            File[] monthFolders = year.listFiles();
                            if(monthFolders != null) {
                                for(File month : monthFolders) {
                                    try {
                                        int currentMonth = Integer.parseInt(month.getName());
                                        if(!monthIsIncluded(currentYear, currentMonth, startDate, endDate))
                                            continue;

                                        if(untar) {
                                            File[] archivedFiles = month.listFiles();
                                            if(archivedFiles == null)
                                                continue;
                                            for(File file : archivedFiles) {
                                                if(file.getName().toLowerCase().startsWith("if" + interfaceId + "_")) {
                                                    System.out.println("Copying file " + file.getName() + "...");
                                                    File newLocation = new File(outLocation.getAbsolutePath() + "/" + file.getName());
                                                    System.out.println("cp " + file.getAbsolutePath() + " " + newLocation.getAbsolutePath());
                                                    Process p = Runtime.getRuntime().exec("cp " + file.getAbsolutePath() + " " + newLocation.getAbsolutePath());
                                                    try { p.waitFor(); } catch(Exception e){
                                                        e.printStackTrace();
                                                    }
                                                    if(p.exitValue() != 0) {
                                                        System.out.println("Unable to move " + file.getAbsolutePath() + "...");
                                                        continue;
                                                    }

                                                    File tmpOut = new File(outLocation.getAbsolutePath() + "/tmp/");
                                                    if(!tmpOut.exists()) {
                                                        if(!tmpOut.mkdir()) {
                                                            System.out.println("Unable to make out folder...");
                                                            continue;
                                                        }
                                                    }

                                                    String[] untarCommand = {"./untarFiles.sh", newLocation.getAbsolutePath(), outLocation.getAbsolutePath() + "/tmp/"};
                                                    String slate = "";
                                                    for(String s : untarCommand)
                                                        slate = slate + s + " ";
                                                    //System.out.println(slate);

                                                    System.out.print("Untarring files...");
                                                    p = Runtime.getRuntime().exec(untarCommand);
                                                    //must read input or deadlock occurs per Process specification JDK 1.7
                                                    InputStream pis = p.getInputStream();
                                                    int b;
                                                    while((b = pis.read()) != -1);
                                                    try { p.waitFor(); } catch(Exception e){
                                                        e.printStackTrace();
                                                    }
                                                    if(p.exitValue() != 0)
                                                        System.out.println("Unable to untar files...");

                                                    System.out.println("Done");

                                                    System.out.print("Recursive move started...");
                                                    p = Runtime.getRuntime().exec("./recursiveMove.sh " + outLocation.getAbsolutePath() + "/tmp/" + " " + outLocation.getAbsolutePath());
                                                    pis = p.getInputStream();
                                                    //must read output or deadlock occurs
                                                    //while((b = pis.read()) != -1);
                                                    try { p.waitFor(); } catch(Exception e){
                                                        e.printStackTrace();
                                                    }
                                                    System.out.println("Done");

                                                    try {
                                                        newLocation.delete();
                                                    } catch(Exception e) {}
                                                }
                                            }
                                        }
                                        else {
                                            File[] archivedMonth = month.listFiles();
                                            if(archivedMonth == null)
                                                continue;
                                            for(File dayFolder : archivedMonth) {
                                                try {
                                                    int currentDay = Integer.parseInt(dayFolder.getName());
                                                    if(!dayIsIncluded(currentYear, currentMonth, currentDay, startDate, endDate))
                                                        continue;

                                                    if(dayFolder.isDirectory()) {
                                                        File[] dataFiles = dayFolder.listFiles();
                                                        for(File file : dataFiles) {
                                                            System.out.println("Copying file " + file.getName() + "...");
                                                            File newLocation = new File(outLocation + "/" + file.getName());
                                                            copyFile(file, newLocation);
                                                        }
                                                    }
                                                }
                                                catch(NumberFormatException e) {}
                                            }
                                        }
                                    }
                                    catch (NumberFormatException e) {
                                        System.out.println("Number Format Exception");
                                    }
                                    catch (IOException e) {
                                        System.out.println("IOException: ");
                                        e.printStackTrace();
                                    }

                                }
                            }
                        } catch(NumberFormatException exception) {}
                    }
                }
            }
        } else
            System.out.println("Nothing to see here.");
    }

    public static void processMakeHL7(String sourceFolderName, String outputFileName) {
        System.out.println("Making CSV from HL7 files in " + sourceFolderName + "...");
        File sourceFolder = new File(sourceFolderName);
        if(!sourceFolder.exists() || !sourceFolder.isDirectory()) {
            System.out.println("Directory " + sourceFolderName + " does not exist. Exiting...");
            System.exit(1);
        }

        File[] fileList = sourceFolder.listFiles();

        try {
            //HashMap<String, Integer> patientMap = new HashMap<String, Integer>();
            BufferedWriter out = new BufferedWriter(new FileWriter(outputFileName));
            CSVWriter csvOut = new CSVWriter(out);
            FileReader fileReader = null;
            for (int x = 0; x < fileList.length; x++) {
                try {
                    fileReader = new FileReader(fileList[x].getAbsolutePath());
                    Hl7InputStreamMessageIterator iter = new Hl7InputStreamMessageIterator(fileReader);

                    while (iter.hasNext()) {
                        ORU_R01 myMsg = (ORU_R01)iter.next();
                        String patFirstName = myMsg.getRESPONSE().getPATIENT().getPID().getPid5_PatientName()[0].getGivenName().toString();
                        String patLastName = myMsg.getRESPONSE().getPATIENT().getPID().getPid5_PatientName()[0].getFamilyName().toString();
                        String patDobString = myMsg.getRESPONSE().getPATIENT().getPID().getDateOfBirth().getTs1_TimeOfAnEvent().toString();
                        String fileName = fileList[x].getName();

                    /* Check to see how many times a patient shows up in group of files
                    String uniqueIdentifier = patFirstName + patLastName + patDobString;
                    Integer j;
                    if((j = patientMap.get(uniqueIdentifier)) == null)
                        patientMap.put(uniqueIdentifier, 1);
                    else
                        patientMap.put(uniqueIdentifier, j + 1);
                    */

                        List<ORU_R01_ORDER_OBSERVATION> orderObservations = myMsg.getRESPONSE().getORDER_OBSERVATIONAll();
                        for(int i = 0; i < orderObservations.size(); i ++) {
                            //trim string to first 8 digits (yyyymmdd)
                            String labDatePerformed = orderObservations.get(i).getOBSERVATION().getOBX().getObx14_DateTimeOfTheObservation().getTs1_TimeOfAnEvent().toString().substring(0,8);
                            /*String labValue = orderObservations.get(i).getOBSERVATION().getOBX().getObx5_ObservationValue(0).getData().toString();
                            if(labValue == null || labValue == "")
                                continue;*/
                            String[] csvString = {patFirstName, patLastName, patDobString, labDatePerformed, fileName};
                            csvOut.writeNext(csvString);
                        }
                    }
                } catch(Exception e) {
                    System.out.println("Exception encountered.");
                    e.printStackTrace();
                } finally {
                    fileReader.close();
                }
            }

            csvOut.close();
            /* Check to see how many times a patient shows up in a group of files
            Collection<Integer> values = patientMap.values();
            int count = 0;
            for(Integer value : values)
                if(value > 1)
                    count += value - 1;
            System.out.println("Duplicate Files(out of " + fileList.length + "): " + count);
            */

            System.out.println("Done.");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Unable to create output file. Exiting...");
            System.exit(1);
        }
    }

    public static void processMakeX12(String sourceFolderName, String outputFileName) {
        System.out.println("Making CSV from HL7 files in " + sourceFolderName + "...");
        File sourceFolder = new File(sourceFolderName);
        if(!sourceFolder.exists() || !sourceFolder.isDirectory()) {
            System.out.println("Directory " + sourceFolderName + " does not exist. Exiting...");
            System.exit(1);
        }

        File[] fileList = sourceFolder.listFiles();

        try {
            //HashMap<String, Integer> patientMap = new HashMap<String, Integer>();
            BufferedWriter out = new BufferedWriter(new FileWriter(outputFileName));
            CSVWriter csvOut = new CSVWriter(out);
            FileReader fileReader = null;
            for (int x = 0; x < fileList.length; x++) {
                try {
                    fileReader = new FileReader(fileList[x].getAbsolutePath());
                    Hl7InputStreamMessageIterator iter = new Hl7InputStreamMessageIterator(fileReader);

                    while (iter.hasNext()) {
                        ORU_R01 myMsg = (ORU_R01)iter.next();
                        String patFirstName = myMsg.getRESPONSE().getPATIENT().getPID().getPid5_PatientName()[0].getGivenName().toString();
                        String patLastName = myMsg.getRESPONSE().getPATIENT().getPID().getPid5_PatientName()[0].getFamilyName().toString();
                        String patDobString = myMsg.getRESPONSE().getPATIENT().getPID().getDateOfBirth().getTs1_TimeOfAnEvent().toString();
                        String fileName = fileList[x].getName();

                    /* Check to see how many times a patient shows up in group of files
                    String uniqueIdentifier = patFirstName + patLastName + patDobString;
                    Integer j;
                    if((j = patientMap.get(uniqueIdentifier)) == null)
                        patientMap.put(uniqueIdentifier, 1);
                    else
                        patientMap.put(uniqueIdentifier, j + 1);
                    */

                        List<ORU_R01_ORDER_OBSERVATION> orderObservations = myMsg.getRESPONSE().getORDER_OBSERVATIONAll();
                        for(int i = 0; i < orderObservations.size(); i ++) {
                            //trim string to first 8 digits (yyyymmdd)
                            String labDatePerformed = orderObservations.get(i).getOBSERVATION().getOBX().getObx14_DateTimeOfTheObservation().getTs1_TimeOfAnEvent().toString().substring(0,8);
                            /*String labValue = orderObservations.get(i).getOBSERVATION().getOBX().getObx5_ObservationValue(0).getData().toString();
                            if(labValue == null || labValue == "")
                                continue;*/
                            String[] csvString = {patFirstName, patLastName, patDobString, labDatePerformed, fileName};
                            csvOut.writeNext(csvString);
                        }
                    }
                } catch(Exception e) {
                    System.out.println("Exception encountered.");
                    e.printStackTrace();
                } finally {
                    fileReader.close();
                }
            }

            csvOut.close();
            /* Check to see how many times a patient shows up in a group of files
            Collection<Integer> values = patientMap.values();
            int count = 0;
            for(Integer value : values)
                if(value > 1)
                    count += value - 1;
            System.out.println("Duplicate Files(out of " + fileList.length + "): " + count);
            */

            System.out.println("Done.");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Unable to create output file. Exiting...");
            System.exit(1);
        }
    }

    public static void processMakeDelimited(String sourceFolderName, String outputFileName, String delimiter, String domain) {
        System.out.println("Making CSV from Delimited files in " + sourceFolderName + "...");
        System.out.println("Using delmiter: " + delimiter);
        File sourceFolder = new File(sourceFolderName);
        if(!sourceFolder.exists() || !sourceFolder.isDirectory()) {
            System.out.println("Directory " + sourceFolderName + " does not exist. Exiting...");
            System.exit(1);
        }

        File[] sourceFiles = sourceFolder.listFiles();

        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(outputFileName));
            CSVWriter csvOut = new CSVWriter(out);
            int errorCount = 0, messageCount = 0, debugCount = 0;
            for(File f : sourceFiles) {
                Scanner sc = new Scanner(f);
                String fileName = f.getName();
                while(sc.hasNextLine()) {
                    messageCount++;
                    String line = sc.nextLine();
                    String msg[] = line.split(delimiter);

                    try{
                        if(!msg[4].equalsIgnoreCase("\"" + domain + "\"") && !msg[4].equalsIgnoreCase(domain))
                            continue;
                        debugCount++;
                        String patientFirstName = msg[1].replaceAll("\"", "");
                        String patientLastName = msg[2].replaceAll("\"", "");
                        String patientDobString = msg[3].replaceAll("\"", "");
                        String labDatePerformed = msg[7].replaceAll("\"","");

                        String[] csvString = {patientFirstName, patientLastName, patientDobString, labDatePerformed, fileName};
                        csvOut.writeNext(csvString);
                    } catch(ArrayIndexOutOfBoundsException e) {
                        errorCount++;
                        continue;
                    }
                }
                csvOut.flush();
                sc.close();
            }
            csvOut.close();
            System.err.println("ArrayIndexOutOfBoundException count: " + errorCount);
            System.out.println("Debug count: " + debugCount);
            System.out.println("Message count: " + messageCount);
            System.out.println("Done.");
        } catch(IOException e) {
            e.printStackTrace();
            System.err.println("Unable to create output file. Exiting...");
            System.exit(1);
        }
    }

    public static LinkedList<String> splitX12Messages(String data) {
        LinkedList splitMessages = new LinkedList();
        Pattern ISACodeRegex = Pattern.compile("ISA[*><|,]00[*><|,]");

        LinkedList<String> allMatches = new LinkedList<String>();
        Matcher m = ISACodeRegex.matcher(data);
        while (m.find())
            allMatches.add(m.group());

        String[] ISAs = ISACodeRegex.split(data);
        if(ISAs.length == 1)
            return null;

        for(int i = 0; i < allMatches.size(); i++) {
            String ISAInterchange = allMatches.get(i) + ISAs[i+1];
            StringReader inData = new StringReader(ISAInterchange.trim());
            StringWriter outData = new StringWriter();

            try {
                EDItoXML x12ToXml = new EDItoXML(inData, outData);
                x12ToXml.setNamespaceEnabled(false);
                x12ToXml.setRecover(true);
                x12ToXml.run();
            } catch(Exception e) {
                e.printStackTrace(System.err);
                continue;
            }

            String XMLString = outData.toString().trim();
            System.out.println(XMLString);

            try {
                DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                Document document = dBuilder.parse(new ByteArrayInputStream(XMLString.getBytes("UTF-8")));

                //Get root element of DOM
                document.getDocumentElement().normalize();
                Element root = document.getDocumentElement();

                //Iterate through DOM to get "loop" node list
                Element interchange = (Element) root.getElementsByTagName("interchange").item(0);
                Element group = (Element) interchange.getElementsByTagName("group").item(0);
                Element transaction = (Element) group.getElementsByTagName("transaction").item(0);
                NodeList loops = transaction.getElementsByTagName("loop");

                Document outDoc = dBuilder.parse(new ByteArrayInputStream("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><data/>".getBytes("UTF-8")));
                for(int x = 0; x < loops.getLength(); x++) {
                    Node importedNode = outDoc.importNode(loops.item(x), true);
                    Element loop = (Element) outDoc.getDocumentElement().appendChild(importedNode);
                    if(loop.hasChildNodes() && (loop.getAttribute("id").equals("2000"))) {
                        NodeList innerLoop = loop.getElementsByTagName("loop");
                        for(int y = 0; y < innerLoop.getLength(); y++) {
                            if(((Element)innerLoop.item(y)).getAttribute("id").equals("2300"))
                                splitMessages.add(outDoc.getTextContent());
                        }
                    }
                }

                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();
                DOMSource source = new DOMSource(outDoc);
                StreamResult result = new StreamResult(new File("out.xml"));

                transformer.transform(source, result);
            } catch(Exception e) {
                e.printStackTrace(System.err);
                continue;
            }

        }
        return splitMessages;
    }

    public static void copyFileFromList(String inputFileName, String inputFolderName, String outputFolderName) {
        System.out.println("Copying files...");
        try {
            Scanner sc = new Scanner(new File(inputFileName));
            while(sc.hasNextLine()) {
                try {
                    String line = sc.nextLine();
                    File file = new File((!inputFolderName.endsWith("/") ? inputFolderName + "/" : inputFolderName) + line);
                    File newFile = new File((!outputFolderName.endsWith("/") ? outputFolderName + "/" : outputFolderName) + line);
                    copyFile(file, newFile);
                } catch(Exception e) {
                    //File doesn't exist, skip
                }
            }
        } catch(IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
        System.out.println("Done.");
    }

    public static void copyFile(File fromFile, File toFile) throws IOException {
        InputStream inStream = new FileInputStream(fromFile);
        OutputStream outStream = new FileOutputStream(toFile);

        byte[] buffer = new byte[1024];
        int length;
        //copy the file content in bytes
        while ((length = inStream.read(buffer)) > 0)
            outStream.write(buffer, 0, length);

        inStream.close();
        outStream.close();
    }
}
