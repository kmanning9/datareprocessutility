#!/bin/bash

find $1 -type f -name '*' -exec mv -i {} $2 \;
rm -r $1

exit 0
